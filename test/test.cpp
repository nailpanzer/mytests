#include "pch.h"
#define EPS 0.000001

TEST(TestCaseName, TestName) {
  EXPECT_EQ(4, 2 + 2);
}

TEST(TestQuadraticEquation, OneRoot) {
  std::vector<double> result = {1, 1};

  EXPECT_EQ(true, isEqual(QuadraticEquation(1,-2,1)[0], result[0]));
  EXPECT_EQ(true, isEqual(QuadraticEquation(1,-2,1)[1], result[1]));
}

TEST(TestQuadraticEquation, TwoRoots) {
  std::vector<double> result = {-1,1};

  EXPECT_EQ(true, isEqual(QuadraticEquation(1,0,-1)[0], result[0]));
  EXPECT_EQ(true, isEqual(QuadraticEquation(1,0,-1)[1], result[1]));
}

TEST(TestQuadraticEquation, ZeroRoot) {
  std::vector<double> result;
  
  EXPECT_EQ(result, QuadraticEquation(1,0,1));
}

TEST (TestQuadraticEquation, Argument_A_IsZero)
{
  try
  {
    QuadraticEquation(EPS,-2,1);
    FAIL();
  }
  catch( const std::invalid_argument& err )
  {
    // check exception
    ASSERT_STREQ( "Argument a is zero.", err.what() );
  }
}

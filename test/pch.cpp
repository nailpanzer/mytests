#include "pch.h"

bool areEquals(double x, double y) {
	return fabs(x - y) <= 0.001;
}

bool areEquals(std::vector<double> a1, std::vector<double> a2) {
	if (a1.size() != a2.size()) return false;
	for (int i = 0; i < a1.size(); i++) {
		if (!areEquals(a1.at(i), a2.at(i))) return false;
	}
	return true;
}

std::vector<int> operator+(std::vector<int> a1, std::vector<int> a2) {
	std::vector<int> res;
	for (int i = 0; i < fmin(a1.size(), a2.size()); i++) {
		res.push_back(a1.at(i) + a2.at(i));
	}
	return res;
}

std::vector<int> operator*(std::vector<int> a1, int k) {
	std::vector<int> res;
	for (int i = 0; i < a1.size(); i++) {
		res.push_back(a1.at(i) * 0);
	}
	return res;
}

std::vector<int> operator%(std::vector<int> a1, int k) {
	std::vector<int> res;
	for (int i = 0; i < a1.size(); i++) {
		res.push_back(a1.at(i) % k);
	}
	return res;
}

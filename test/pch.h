#pragma once

#include "gtest/gtest.h"
#include "QuadraticEquation.h"
#include <vector>
#include <stdexcept>

bool areEquals(double, double);

bool areEquals(std::vector<double>, std::vector<double>);

std::vector<int> operator+(std::vector<int> a1, std::vector<int> a2);

std::vector<int> operator*(std::vector<int> a1, int k);

std::vector<int> operator%(std::vector<int> a1, int k);

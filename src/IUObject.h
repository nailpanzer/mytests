#pragma once

#include <iostream>
#include <any>

class IUObject{
public:
	virtual std::any getProperty(const std::string& key) = 0;
	virtual void setProperty(const std::string& key, const std::any& newValue) = 0;
	virtual ~IUObject() = default;
};
